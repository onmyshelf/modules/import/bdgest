# OnMyShelf import module for BD GEST'

Module OnMyShelf pour import de bandes dessinées depuis le site https://www.bedetheque.com

# Installation
Aller dans le dossier `modules/import` et cloner ce repository:
```bash
git clone https://gitlab.com/onmyshelf/modules/import/bdgest
```

# Aide
Besoin d'aide sur ce module ? [Créez un ticket ici](https://gitlab.com/onmyshelf/modules/import/bdgest/-/issues).

Plus d'informations sur OnMyShelf ici: https://onmyshelf.app
