<?php

require_once('inc/import/global/html.php');

class Import extends HtmlImport
{
    public function load()
    {
        $this->website = 'https://m.bedetheque.com';

        $this->properties = [
            'source',
            'title',
            'series',
            'cover',
            'author',
            'summary',
            'reference',
            'isbn',
            'editor',
            'date',
            'pages',
            'format',
        ];

        return true;
    }


    /**
     * Search item
     * @param  string $search
     * @return array
     */
    public function search($search)
    {
        $this->source = $this->website."/album?RechTitre=".preg_replace('/\s/', '+', $search);
        $this->loadHtml();

        $results = [];
        foreach ($this->html->find('ul.li-album li') as $item) {
            // ignore line separators in results
            if (isset($item->{'data-role'}) && $item->{'data-role'} == 'list-divider') {
                continue;
            }

            $results[] = [
                'source' => $this->getLink('a', $item),
                'name' => $this->getText($this->getHtml('h2', $item)),
                'image' => $this->getImgSrc('img.couv', $item),
                'description' => $this->getText($this->getHtml('p.titre', $item)),
            ];
        }

        return $results;
    }


    /**
     * Get item data from source
     * @return array
     */
    public function getData()
    {
        $this->loadHtml();

        return [
            'source' => $this->source,
            'title' => $this->getDetails('Titre'),
            'series' => $this->getDetails('Série'),
            'cover' => $this->download($this->getImgSrc('#album-slider img')),
            'author' => $this->getDetails('Scénario'),
            'summary' => $this->getDetails('Résumé'),
            'reference' => $this->getDetails("Identifiant"),
            'isbn' => $this->getDetails("ISBN"),
            'editor' => $this->getDetails("Editeur"),
            'date' => $this->getDetails("Dépot légal"),
            'pages' => $this->getDetails("Planches"),
            'format' => $this->getDetails("Format"),
        ];
    }


    /**
     * Get item details from label
     * @param  string $label
     * @return string
     */
    private function getDetails($label)
    {
        // parse all item details
        foreach ($this->html->find('#album-detail > li') as $detail) {
            $text = $this->getText($detail);

            // if label matches, return value without label prefix
            if (trim(explode(':', $text)[0]) == $label) {
                return preg_replace('/^\S+\s*:\s*/', '', $text);
            }
        }
    }
}
